import flwr as fl
import sys

from utils import load_dataset
from client_class import FlowerClient
from model import model

# Load dataset
# dist here is the distribution of the dataset for multiple clients
dist = [4000, 4000, 4000, 3000, 10, 10, 10, 10, 4000, 10]
x_train, y_train, x_test, y_test = load_dataset(dist)

# Start Flower client
fl.client.start_numpy_client(
    server_address="localhost:" + str(sys.argv[1]),
    client=FlowerClient(model(), x_train, y_train, x_test, y_test),
    grpc_max_message_length=1024 * 1024 * 1024
)
