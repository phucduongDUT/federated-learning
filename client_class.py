import flwr as fl


class FlowerClient(fl.client.NumPyClient):
    def __init__(self, model, x_train, y_train, x_test, y_test):
        self.model = model
        self.x_train = x_train
        self.y_train = y_train

        self.x_test = x_test
        self.y_test = y_test

    def get_parameters(self, config):
        return self.model.get_weights()

    def fit(self, parameters, config):
        self.model.set_weights(parameters)
        r = self.model.fit(self.x_train, self.y_train, epochs=1, validation_data=(self.x_test, self.y_test), verbose=0)
        hist = r.history
        print("Fit history : ", hist)
        return self.model.get_weights(), len(self.x_train), {}

    def evaluate(self, parameters, config):
        self.model.set_weights(parameters)
        loss, accuracy = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        print("Eval accuracy : ", accuracy)
        return loss, len(self.x_test), {"accuracy": accuracy}
