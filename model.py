from tensorflow import keras


def model():
    m = keras.Sequential([
        keras.layers.Flatten(input_shape=(28, 28)),
        keras.layers.Dense(128, activation='relu'),
        keras.layers.Dense(256, activation='relu'),
        keras.layers.Dense(10, activation='softmax')
    ])
    m.compile("adam", "sparse_categorical_crossentropy", metrics=["accuracy"])

    return m
